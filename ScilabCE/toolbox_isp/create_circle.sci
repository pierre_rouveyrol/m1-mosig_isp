function image=create_circle(radius, imageSize)
    a = floor(-imageSize / 2);
    b = floor((imageSize-1) / 2);
    X = ones(imageSize,1)*[a:b];
    Y = [a:b]'*ones(1,imageSize);
    Z = X.^2 + Y.^2;
    image = zeros(imageSize, imageSize);
    image(find(Z <= radius^2)) = 1;
endfunction
