function image=create_gaussian(radius, imageSize)
    nx = imageSize; ny = imageSize;      // defines the number of elements along x and y
    x = linspace(-1, 1, nx); //defines the range
    y = linspace(-1, 1, ny);
    [X, Y] = ndgrid(x, y);   // creates two 2-D arrays of x and y coordinates
    A = zeros(nx, ny);

    r = sqrt(X.^2 + Y.^2); // note element-per-element squaring of X and Y
    A = exp(-r/radius);         // 2D Gaussian
    image = A;
endfunction
