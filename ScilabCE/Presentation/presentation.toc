\beamer@endinputifotherversion {3.31pt}
\beamer@sectionintoc {1}{Principle}{3}{0}{1}
\beamer@sectionintoc {2}{Implementation}{5}{0}{2}
\beamer@sectionintoc {3}{Results}{6}{0}{3}
\beamer@subsectionintoc {3}{1}{Smoothing}{6}{0}{3}
\beamer@subsectionintoc {3}{2}{Sharpening}{10}{0}{3}
\beamer@sectionintoc {4}{Conclusion}{13}{0}{4}
