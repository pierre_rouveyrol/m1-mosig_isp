getd('toolbox_isp\');
In=read_pgm('data/lenaWithNoise.pgm');
scf;
IdKern=create_circle(64,256);
InFt=fft2(In);
sInFt=fftshift(InFt);
OutFt=sInFt.*IdKern;
Out=abs(ifft2(OutFt));
xtitle('Lena Ideal Filter','x','y')
imageplot(Out);
