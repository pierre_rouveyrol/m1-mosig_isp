getd('toolbox_isp\');
In=read_pgm('data/sampleImage.pgm');
scf;
IdKern=create_circle(32,500);
IdKern=abs(IdKern-255);
InFt=fft2(In);
sInFt=fftshift(InFt);
OutFt=sInFt.*IdKern;
Out=abs(ifft2(OutFt));
xtitle('Lena Ideal Filter','x','y')
imageplot(Out);
