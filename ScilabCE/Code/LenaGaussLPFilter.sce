getd('toolbox_isp\');
In=read_pgm('data/lenaWithNoise.pgm');
scf;
GaussKern=create_gaussian(32,256);
InFt=fft2(In);
sInFt=fftshift(InFt);
OutFt=sInFt.*GaussKern;
Out=abs(ifft2(OutFt));
xtitle('Lena Gauss LP Filter','x','y')
imageplot(Out);
