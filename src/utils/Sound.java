/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.*;
import javax.sound.sampled.*;

import lab.Signal;

/**
 *
 * @author cfouard
 */
public class Sound {

  private AudioFormat format;
  private byte[] samples;

  public Sound(String filename) {
    try {
      AudioInputStream stream = AudioSystem.getAudioInputStream(new File(filename));
      format = stream.getFormat();
      samples = getSamples(stream);
    } catch (UnsupportedAudioFileException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public byte[] getSamples() {
    return samples;
  }

  public byte[] getSamples(AudioInputStream stream) {
    int length = (int) (stream.getFrameLength() * format.getFrameSize());
    System.out.println("stream frame length: " + stream.getFrameLength());

    byte[] samples = new byte[length];
    DataInputStream in = new DataInputStream(stream);
    try {
      in.readFully(samples);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return samples;
  }

  public void play(InputStream source) {
    // 100 ms buffer for real time change to the sound stream
    int bufferSize = format.getFrameSize() * Math.round(format.getSampleRate() / 10);
    byte[] buffer = new byte[bufferSize];
    SourceDataLine line;
    try {
      DataLine.Info info = new DataLine.Info(SourceDataLine.class, format);
      line = (SourceDataLine) AudioSystem.getLine(info);
      line.open(format, bufferSize);
    } catch (LineUnavailableException e) {
      e.printStackTrace();
      return;
    }
    line.start();
    try {
      int numBytesRead = 0;
      while (numBytesRead != -1) {
        numBytesRead = source.read(buffer, 0, buffer.length);
        if (numBytesRead != -1) {
          line.write(buffer, 0, numBytesRead);
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    line.drain();
    line.close();
  }

  public Signal toSignal(int nbSamples) {
    Signal signal = new Signal();
    double sampleRate = format.getFrameRate() * format.getFrameSize();
    if (nbSamples == -1 || nbSamples > samples.length) {
      for (int i = 0; i < samples.length; i++) {
        signal.addElement(i / sampleRate, (double) samples[i]);
      }
    } else {
      for (int i = 0; i < nbSamples; i++) {
        signal.addElement(i / sampleRate, (double) samples[i]);
      }

    }
    System.out.println("One sample every " + 1/sampleRate + " second.");
    return signal;
  }

  public static void main(String[] args) {
//    Sound player = new Sound("data/a.wav");
//    InputStream stream = new ByteArrayInputStream(player.getSamples());
//    player.play(stream);
    /*
    System.out.println("Sample rate: " + player.format.getSampleRate());
    System.out.println("Number of samples: " + player.samples.length);
    System.out.println("Frame Size: " + player.format.getFrameSize());
     */
    System.exit(0);
  }
}
