/*
 * Please modify this class and complete the following methods corresponding
 * to your computer exercise.
 */
package lab;

import java.util.ArrayList;
import java.lang.Math;

/**
 *
 * @author your name
 */
public class Signal extends GeneralSignal {

  public Signal() {
    super();
  }

  public Signal(String filename) {
    super(filename);
  }

  /* ************************************************************************* */
  /*                                                                           */
  /*         Computer Exercise number 01                                       */
  /*                                                                           */
  /* ************************************************************************* */
  /**
   * Computes and returns the mean value of the signal.
   * @return mean value of the signal.
   */
  public double getMean() {
    int nbSamples = this.getNbSamples();
    double mean = 0.0;
    for(int i=0;i<nbSamples;i++){
        mean=mean+this.getValueOfIndex(i);
    }
    mean=mean/nbSamples;
    return mean;
  }

  /**
   * Computes and returns the standard deviation (&#963;) of the signal.
   * @return Standard deviation (&#963;) of the signal.
   */
  public double getStandardDev() {
    int nbSamples = this.getNbSamples();
    double standardDev = 0.0;
    double mean=getMean();
    for(int i=0;i<nbSamples;i++){
        standardDev=standardDev+Math.pow(getValueOfIndex(i)-mean,2);
    }
    standardDev=standardDev/(nbSamples);
    standardDev=Math.sqrt(standardDev);
    
    return standardDev;
  }

  public double getVariance() {
    int nbSamples = this.getNbSamples();
    double variance = 0.0;
    // Write your code here

    return variance;
  }

  /**
   * Computes the histogramm of a signal for a given interval length.<br/>
   * This means that this function:
   *  - divides the value interval interval of the signal
   * ([getMin(), getMax()]) into N intervals of length {@link intervalLength}
   * N = ???.
   * <br/>
   * <ul>
   *    <li>for each interval of length intervalLength, counts the number of
   * signal samples which value/ordinate is within the corresponding interval.
   *    </li>
   *    <li>stores this value at the ascissa corresponding to the center of the
   * interval.
   *    </li>
   *
   * </ul>
   * @param intervalLength length of each interval
   * @return a signal containing the histogram of the current signal.
   */

public Signal getHitogram(double intervalLength) {
   Signal histo = new Signal();    
   if (intervalLength <= 0.0) {
     System.out.println("Error in computeHistogram method from Signal:\n The length of an interval must be strictly positive.\n");
     return null;
   }
   int numberOfIntervals;
   numberOfIntervals=(int)((getMax()-getMin())/intervalLength)+1;
   int intervals[] = new int[numberOfIntervals];
//      System.out.println(numberOfIntervals);
   java.util.Arrays.fill(intervals,0);
   int current;
   for(int i=0;i<this.getNbSamples();i++){
       current=(int)(Math.abs(getValueOfIndex(i)-getValueOfIndex(0))/intervalLength);
//        System.out.println(current);
       intervals[current]++;
   }
   for(int i=0;i<numberOfIntervals;i++){
       histo.addElement(i,intervals[i]);
   }
   return histo;
 }
  
  public static Signal createRandomSeries(int nbElements) {
    return createRandomSeries(0.0, 1.0, nbElements);
  }
  
  public static Signal createRandomSeries(double min, double max, int nbElements) {
    Signal resultSignal = new Signal();
    resultSignal.settName("Random Signal");

    double randomNumber;
    for (int i = 0; i < nbElements; i++) {
      randomNumber = min + Math.random() * (max - min);
      resultSignal.addElement(i, randomNumber);
    }
    return resultSignal;
  }
  
  
  /* ************************************************************************* */
  /*                                                                           */
  /*         Computer Exercise number 01   Team 01                             */
  /*                                                                           */
  /* ************************************************************************* */
  
  /**
   * Given the number of samples <i>nbSamples</i> (nbSamples = this.getNbSamples()) of the
   * current signal, decomposes the signal in <i>nbSamples</i> <b>impulse</b> component singals.
   * @return an ArrayList containing nbSamples impulse component signals.
   */
  public ArrayList<GeneralSignal> impulseDecomposition() {
    ArrayList<GeneralSignal> list = new ArrayList<GeneralSignal>();

    Signal signal = new Signal();
    list.add(signal);

    // Write your code here

    return list;
  }

  /**
   * Given the number of samples <i>nbSamples</i> (nbSamples = this.getNbSamples()) of the
   * current signal, decomposes the signal in <i>nbSamples</i> <b>step</b> component singals.
   * @return an ArrayList containing nbSamples step component signals.
   */
  public ArrayList<GeneralSignal> stepDecomposition() {
    ArrayList<GeneralSignal> list = new ArrayList<GeneralSignal>();
    // Write your code here

    return list;
  }

  /**
   * Decomposes the current signal into an <b>even signal</b> and an <b>odd signal</b>.
   * @return an ArrayList of 2 signals (one even component signal and one odd component signal).
   */
  public ArrayList<GeneralSignal> evenOddDecomposition() {
    ArrayList<GeneralSignal> list = new ArrayList<GeneralSignal>();
    // Write your code here

    return list;
  }

  /**
   * Decomposes the current signal into an <b>even samples signal</b> and an <b>odd samples signal</b>.
   * @return an ArrayList of 2 signals (one even component signal and one odd component signal).
   */
  public ArrayList<GeneralSignal> interlacedDecomposition() {
    ArrayList<GeneralSignal> list = new ArrayList<GeneralSignal>();
    // Write your code here

    return list;
  }

  /* ************************************************************************* */
  /*                                                                           */
  /*         Computer Exercise number 01   Team 02                             */
  /*                                                                           */
  /* ************************************************************************* */

public static Signal createGaussianNoiseSeries(int nbElements) {
    double val, random;
    Signal resultSignal = new Signal();
    resultSignal.settName("Gaussian Noise");
    for (int n = 0; n < nbElements; n++) {
      resultSignal.addElement(n, 0.0);
      for (int j = 0; j < 12; j++) {
        val = resultSignal.getValueOfIndex(n);
        random = Math.random();
        resultSignal.setValueOf(n, val + random);
      }
      val = resultSignal.getValueOfIndex(n);
      val -= 6;
      resultSignal.setValueOf(n, val);
    }
    return resultSignal;
  }


  /* ************************************************************************* */
  /*                                                                           */
  /*         Computer Exercise number 02                                       */
  /*                                                                           */
  /* ************************************************************************* */
  /**
   * Generates and returns the signal
   * x1[n] = sin(2 &Pi;n / 100)
   * @param nbSamples number of samples of the signal.
   * @return x1[n] = sin(2 &Pi;n / 100)
   */
  public static Signal generateX1(int nbSamples) {
    Signal x1 = new Signal();
    x1.settName("x1[n] = sin(2 Pi n / 100)");
    for (int i=1; i<nbSamples; i++)
    {
        x1.addElement(i, Math.sin(2.0*Math.PI*i / 100));
    }

    return x1;
  }

  /**
   * Generates and returns the signal
   * x2[n] =  4*exp(-(n-150)^2/300) - exp(-(n-150)^2/2500)
   * @param nbSamples number of samples of the signal.
   * @return x2[n] =  4*exp(-(n-150)^2/300) - exp(-(n-150)^2/2500)
   */
  public static Signal generateX2(int nbSamples) {
    Signal x2 = new Signal();
    x2.settName("x2[n] =  4*exp(-(n-150)^2/300) - exp(-(n-150)^2/2500)");
    for (int i=1; i<nbSamples; i++)
    {
        x2.addElement(i, 4.0*Math.exp(-(i-150.0)*(i-150.0) / 300) - Math.exp(-(i-150)*(i-150.0) / 2500));
    }


    return x2;
  }

  /**
   * Generates and returns the signal
   * x3[n] =  1 for 240 &lt; n &gt; 300
   *         -2 for 299 &lt; n &gt; 380
   *          0  otherwise
   * @param nbSamples
   * @return x3
   */
  public static Signal generateX3(int nbSamples) {
    Signal x3 = new Signal();
    x3.settName("x3");
    for (int i=1; i<nbSamples; i++)
    {
        if(240 < i && i < 300)
            x3.addElement(i, 1);
        if(299 < i && i < 380)
            x3.addElement(i, -2);
        else
            x3.addElement(i, 0);
        
    }

    return x3;
  }

  /**
   *
   * Applies the following filter to current signal and returns the resulting signal:
   * y[0] = ??
   * For n from 1 to numberOfSamples-1
   * y[n] = 0.05*x[n] + 0.95*y[n-1]
   *
   */
  public Signal singlePolLowPassFilter() {
    Signal result = new Signal();
    result.settName("Filtered signal");
    result.addElement(0, 0);
    for(int i=1; i<this.getNbSamples(); i++)
        result.addElement(i, 0.05*this.getValueOfAbscissa(i) + 0.95*result.getValueOfIndex(i-1));
    
    return result;
  }

  /**
   * Linear convolve the current signal with the given kernel
   */
  public Signal linearConvolve(Signal kernel) {
    Signal result = new Signal();
    result.settName("Convolved signal");
    // Write your code here

    return result;
  }

  /**
   * Circular convolve the current signal with the given kernel
   */
  public Signal circularConvolve(Signal kernel) {
    Signal result = new Signal();
    result.settName("Convolved signal");
    double xPlush;
    double xnPlusk;
    for(int n=0;n<this.getNbSamples();n++){   
        int k;
        int kk;
        xPlush=0;        
        for(k=-kernel.getNbSamples()/2;k<kernel.getNbSamples()/2+1;k++){
            if(((n+k)<0)||(n+k)>this.getNbSamples()-1){
                 xnPlusk=0;   
            }
            else{
                 xnPlusk=this.getValueOfAbscissa(n+k);  
            }
            kk=(kernel.getNbSamples()+k) % (kernel.getNbSamples());
            xPlush=xPlush+kernel.getValueOfAbscissa(kk)*xnPlusk;
            
        }
        result.addElement(n,xPlush);
    }
    return result;
  }

  
  /* ************************************************************************* */
  /*                                                                           */
  /*         Computer Exercise number 02  Team 03                              */
  /*                                                                           */
  /* ************************************************************************* */
    // Write your additional functions here
  
  /* ************************************************************************* */
  /*                                                                           */
  /*         Computer Exercise number 03   Team 04                             */
  /*                                                                           */
  /* ************************************************************************* */
  // Write your additional functions here
  
  
  /* ************************************************************************* */
  /*                                                                           */
  /*         Computer Exercise number 03    (see Image.java)                   */
  /*                                                                           */
  /* ************************************************************************* */

  
  /* ************************************************************************* */
  /*                                                                           */
  /*         Computer Exercise number 03   Team 06   (see Image.java)          */
  /*                                                                           */
  /* ************************************************************************* */

  /* ************************************************************************* */
  /*                                                                           */
  /*         Computer Exercise number 03   Team 05                             */
  /*                                                                           */
  /* ************************************************************************* */
  public Signal stretchContrast(double newRangeMin, double newRangeMax) {
    Signal signal = new Signal();
    // Write your code here

    return signal;
  }


  /* ************************************************************************* */
  /*                                                                           */
  /*         Computer Exercise number 04                                       */
  /*                                                                           */
  /* ************************************************************************* */
  /**
   * Computes the Discrete Fourier Transform of the current signal
   * @return DFT of the current signal (as ComplexSignal)
   */
  public ComplexSignal dft() {
    ComplexSignal result = new ComplexSignal();
    int nbSamples = this.getNbSamples();
    
    for(int n=0; n < nbSamples; n++){
        Complex x = new Complex();
        for(int k=0; k < nbSamples; k++){
            x.add(Complex.createFromPolar(this.getValueOfAbscissa(k), -2*Math.PI*k*n/nbSamples));
        }
        result.add(x);
    }
    return result;
  }

  /**
   * Computes the Inverse Discrete Fourier Transform of the complex signal given
   * in parameters.<br/>
   * Note: this method should be in ComplexSignal, but for simplicity matters you
   * will not have to modify the class ComplexSignal, so you implement a static
   * method in Signal instead.
   * Note II: Compute the idft with complex number. At the end, take the real
   *   part of your result signal.
   * @param fourier input Fourier transform of the signal
   * @return the inverse Fourier transform of the input complex signal
   */
  public static Signal idft(ComplexSignal fourier) {
    ComplexSignal result = new ComplexSignal();
    int nbSamples = fourier.getNbSamples();
    
    for(int n=0; n < nbSamples; n++){
        Complex x = new Complex();
        for(int k=0; k < nbSamples; k++){
	    Complex temp = fourier.get(k);
	    temp.multiplyByReal(Math.pow(Math.E, -2*Math.PI*k*n/nbSamples));
            x.add(temp);
        }
        result.add(x);
    }
    return result.getRealSignal();
  }

  /**
   * Generates a &delta; signal
   * @param nonZeroSampleNumber number of the sample which value equals 1
   * @param numberOfSamples total number of samples of the result signal
   * @return Delta signal
   */
  public static Signal generateDelta(int nonZeroSampleNumber, int numberOfSamples) {
    Signal signal = new Signal();
    signal.settName("Delta_" + nonZeroSampleNumber);
    
    for(int i=0; i<numberOfSamples; i++){
        if(i!=nonZeroSampleNumber)
            signal.addElement(i,0);
        else
            signal.addElement(i,1);
    }
    return signal;
  }

  /**
   * Generate a Rectangular signal of numberOfSamples samples:
   *  before firstNonZeroSample, and after lastNonZeroSample, sample values are 0.0
   *  between firstNonZeroSample and lastNonZeroSample included, sample values are 1.0
   * @param firstNonZeroSample index of the first non zero sample
   * @param lastNonZeroSample index of the last non zero sample
   * @param numberOfSamples number of samples of the signal
   */
  public static Signal generateRectangle(int firstNonZeroSample, int lastNonZeroSample, int numberOfSamples) {
    Signal signal = new Signal();
    signal.settName("Rectangle_" + firstNonZeroSample + "_" + lastNonZeroSample);
    // Write your code here

    return signal;
  }

  /**
   * Compute a complex signal where for each sample (of index n)
   * - the magnitude is
   *     - m if n=0
   *     - if n>0: sinc_m[n] = sin(2.pi.n/N . (m+1/2)) / sin(pi.n/N)
   *        (where N is the number of samples (numberOfSamples parameter))
   *
   * - the phase is 0
   *
   * @param numberOfSamples number of samples of the signal (N in the equation above)
   * @param m (as defined in the subject)
   * @return
   */
  public static ComplexSignal generateComplexSinc(int numberOfSamples, int m) {
    ComplexSignal signal = new ComplexSignal();
    // Write your code here

    return signal;
  }

  public static Signal generateXSignal(int numberOfSamples) {
    Signal signal = new Signal();
    signal.settName("(sin(2pi n 0.08)+2sin(2pi n 0.3))e^{-(n-200)^2 / 60^2}");
    // Write your code here

    return signal;
  }

  public static Signal generateSin(int nbSamples, double a, int k) {
    Signal signal = new Signal();
    // Write your code here

    return signal;
  }

  public static Signal generateCos(int nbSamples, double a, int k) {
    Signal signal = new Signal();
    // Write your code here

    return signal;
  }

}
